# Parte di `nicb`: Schema degli Argomenti

## Fondamenti di macchine di calcolo

* Prodromi: Leibnitz, Babbage
* schema generale di funzionamento
  * macchina di Turing
  * risolvibilità dei problemi
  * sistema numerico
  * standards: *ASCII*, *UTF-8*, *RGB*, ecc.
  * sequenza di boot

### Hardware

* componenti
  * ram
  * cpu
  * bus
  * storage
  * I/O

### Software

* sistema operativo:
  * gestione centralizzata delle risorse
  * SO monolitici vs. micro-kernel
  * *time-sharing* operation:
    * `user space` vs `kernel space`
    * `tasks`
      * `sleep`
    * `threads`
      * *semafori*
      * `mutex`
* `shell`
  * command-line interface:
    * metafora della macchina da scrivere
  * GUI:
    * metafora del *desktop*
* chiamate sistema

### CLI vs. IDE

### *divide et impera* (*small is beautiful*)

* *everything is a file*
* I/O: stdin, stdout, stderr
* valori di ritorno dei comandi: `0` (nessun problema), `~0` (errore)
* *piping* e *redirections*
  * comandi logici: `&&`, `||`
  * ridirezioni: '<', '>', '2>&1', ecc.

### Emulazione di terminale

### `bash` shell

* *command-line completion*
* bash *globbing*
* variabili di *environment*
  * inizializzazione
  * richiamo
  * variabili speciali:
    * `$PATH` -> elenco di cartelle dei comandi
    * `$PS`* -> prompts
    * `$?` -> valore di ritorno
    * `$@` -> argomenti di un comando
    * `$#` -> numero degli argomenti
    * `$$` -> PID del comando
    * [riferimento](https://www.shell-tips.com/bash/environment-variables/)
* scripting: *hash bang* (`#!`)
* scripts letti all'inizio di una shell: `.profile`, `.bashrc`
* `alias`
* operatori
* funzioni interne alla `bash shell`:
  * funzioni di controllo:
    * `if`-`then`-`else`-`fi`
    * `for`-`do`-`done`
    * `while`-`do`-`done`
* *scripts* eseguibili

### comandi principali

* struttura generale dei comandi (comandi, opzioni, argomenti)
* operatori
* espressioni regolari
  * nei linguaggi: `awk`, `perl`, `ruby`, `python`, ...
* `echo`
* `cat`
* `man`
* `apropos`
* `cd` (nomenclatura)
* `ls`
* `chmod`
* `chown`
* `grep`
* `sed`
* `awk`
* `more`/`less`
* editor di testo *in-terminal*: `vi`/`vim`, `emacs`, `nano`
* editor di testo *out-terminal*: `atom`, `gedit`, ecc.
* `make`

### utilità audio cli

* `sox`
* `lame`
* `ffmpeg`

## Reti

* Modello client-server
  * esempio semplificativo e spiegato
* Architettura multitier
  * separazione degli strati
    * three-tier: presentation, logic, data storage
    * modello MVC (model-view-controller)
  * Esempi:
    * applicazioni web - con estensioni *app*
* API e interfacce di astrazione
* Protocolli (con esempi: `HTTP`, `DNS`, `DHCP`...)
* Formato dei pacchetti
* Le pile TCP/IP e UDP/IP
* Classificazione delle reti in base all'estensione geografica
* Classificazione delle reti in base al canale di trasmissione
* (Classificazione delle reti in base alla topologia)
* Il modello di indirizzamento IPV4
* Il modello di indirizzamento IPV6
* I dispositivi di rete (scheda LAN, modem, access point, router...)

### utilità di servizio

* sistemi di SCM (*source code management*): `git`, `svn`, `hg`, ...
* sistemi di CI (*continuous integration*): `travis`, `circle`, `jenkins`, ...
* *containers*: `docker`, `kubernetes`, ...
* *clouds*: `paas`, `saas`, `iaas`, ...

## Ecosistema di sviluppo

### Software Libero

* definizioni e distinguo
* sorgenti
* sistemi operativi
* sistemi di packaging

## Programmazione

### Linguaggi

* compilatori e interpreti
* ibridi (java, scala, python, jython, jruby, ecc.)
* meta-compilatori
* *domain-specific languages* (DSL)

### Fondamenti

* perché esistono i linguaggi di programmazione?
* programmazione strutturata
* programmazione a oggetti
* riutilizzabilità del codice
  * librerie statiche
  * librerie dinamiche
* codice ibrido

### Come si programma

* Agile Programming
* Test-Driven Development
* Behaviour-Driven Development

### Strutture e algoritmi

* scalari
  * tipi naturali
  * ambito operativo (`scope`)
* *array*
* puntatori
* *array* associativi
* controllo di flusso
  * condizionali
  * iteratori
* stringhe come *array* di caratteri
* *stack*
* *liste linkate*
* strutture dati
* classi
  * definizioni
  * ereditarietà (singole e multiple)
  * istanze
  * proprietà d'istanza
  * proprietà di classe
  * metodi d'istanza
  * metodi di classe
  * incapsulamento e visibilità
  * classi celibi (*singleton*)

### Linguaggi

#### `C`/`C++`

#### `python`

#### `ruby`

#### altri linguaggi generici

* `lua`
* `javascript`
* `go`
* `java`
* ~~`processing`~~

#### linguaggi specifici

* ~~`csound`~~
* ~~`supercollider`~~
* ~~`lilypond`~~
* ~~`faust`~~
* `pic`

### Programmazione grafica

* strutturazione dei programmi
* toolkits
