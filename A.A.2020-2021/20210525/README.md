# Bernardini - Fondamenti di Informatica - Seminario del 25/05/2021

## [YouTube video](https://youtu.be/mD-zHOkpVbY)

## Programmazione

### `Javascript`: [fondamenti](./primo_tentativo.js)

```javascript
//1

console.log('ciao');
//2
function dici_ciao()
 {
console.log('dici ciao');


};

dici_ciao();

//3

function somma(a,b)
{
var c='ciao'; 
console.log(a+b+c);

};

somma(8,6.2);


//4
var id=
{nome:'Leonardo',
cognome:'Polla',
eta:19


};
console.log(id.nome, id.cognome, id.eta);

console.log(id['nome'])


//5
function Id(nome,cognome,eta)
{this.nome=nome;
    this.cognome=cognome;
    this.eta=eta;
    this.display=function() {console.log('ciao '+this.nome+'!')}

};
//6
var Leo = new Id('Leo','Polla',19);
//Leo.display();
function studente(nome,cognome,eta,matricola)
{Id.call(this,nome,cognome,eta);
    this.matricola=matricola;
    this.display=function(){console.log(`ciao ${this.nome}! sei la matricola ${this.matricola}`)}

}
var LeoS = new studente('Leo','Polla',19,2098);
LeoS.display();
```

```sh
$ node primo_tentativo.js
ciao
dici ciao
14.2ciao
Leonardo Polla 19
Leonardo
ciao Leo! sei la matricola 2098
```

### Conversione [score di csound](./output_score.sco) a partitura grafica

Dati i primi 60 secondi di [questa](./output_score.sco) partitura `csound`:

```csound
f1 0 4096 10 1
f2 0 4096 10 1 .9 .8 .7 .6 .5 .4 .3 .2 .1
;i2 0 60
i003  20.0000   0.1500   0.0200 190.7400  10.0000   1.0000   1.0000 ; this_tempo: 40.0000, cur_metro: 1.5000, cur_step: 2.0000, 
i003  20.0000   0.1500   0.0200 170.7400  10.0000   1.0000   1.0000 ; this_tempo: 120.0000, cur_metro: 0.5000, cur_step: 0.5000, 
i003  20.5000   0.1500   0.0200 175.7400   1.0000   1.0000   1.0000 ; this_tempo: 100.0000, cur_metro: 0.6000, cur_step: 0.6000, 
i003  21.0000   0.1500   0.0200 180.7400   0.1000   1.0000   1.0000 ; this_tempo: 80.0000, cur_metro: 0.7500, cur_step: 1.0000, 
i003  21.0000   0.1500   0.0200 180.7400   0.1000   1.0000   1.0000 ; this_tempo: 80.0000, cur_metro: 0.7500, cur_step: 0.7500, 
i003  21.3000   0.1500   0.0200 180.7400   0.1000   1.0000   1.0000 ; this_tempo: 80.0000, cur_metro: 0.7500, cur_step: 1.0000, 
i003  21.3000   0.1500   0.0200 180.7400   0.1000   1.0000   1.0000 ; this_tempo: 80.0000, cur_metro: 0.7500, cur_step: 0.7500, 
i003  21.5000   0.1500   0.0200 180.7400   0.1000   1.0000   1.0000 ; this_tempo: 80.0000, cur_metro: 0.7500, cur_step: 1.0000, 
i003  21.5000   0.1500   0.0200 180.7400   0.1000   1.0000   1.0000 ; this_tempo: 80.0000, cur_metro: 0.7500, cur_step: 0.7500, 
i003  22.2500   0.1500   0.0200 173.2400   3.1623   1.0000   1.0000 ; this_tempo: 110.0000, cur_metro: 0.5455, cur_step: 0.5455, 
i003  50.0000   0.2000   0.0200 514.7300  20.0000   1.0000   1.0000 ; this_tempo: 40.0000, cur_metro: 1.5000, cur_step: 0.7500, 
i003  50.0000   0.3000   0.0200 454.7300  20.0000   1.0000   1.0000 ; this_tempo: 120.0000, cur_metro: 0.5000, cur_step: 0.3333, 
i003  50.3333   0.3000   0.0200 474.7300   0.5848   1.0000   1.0000 ; this_tempo: 93.3333, cur_metro: 0.6429, cur_step: 0.4286, 
i003  50.5000   0.2000   0.0200 484.7300   0.1000   1.0000   1.0000 ; this_tempo: 80.0000, cur_metro: 0.7500, cur_step: 0.3750, 
i003  50.5000   0.3000   0.0200 484.7300   0.1000   1.0000   1.0000 ; this_tempo: 80.0000, cur_metro: 0.7500, cur_step: 0.5000, 
i003  50.7000   0.2000   0.0200 484.7300   0.1000   1.0000   1.0000 ; this_tempo: 80.0000, cur_metro: 0.7500, cur_step: 0.3750, 
i003  50.7000   0.3000   0.0200 484.7300   0.1000   1.0000   1.0000 ; this_tempo: 80.0000, cur_metro: 0.7500, cur_step: 0.5000, 
i003  51.0000   0.2000   0.0200 484.7300   0.1000   1.0000   1.0000 ; this_tempo: 80.0000, cur_metro: 0.7500, cur_step: 0.3750, 
i003  51.0000   0.3000   0.0200 484.7300   0.1000   1.0000   1.0000 ; this_tempo: 80.0000, cur_metro: 0.7500, cur_step: 0.5000, 
i003  90.0000   0.2000   0.0500 346.6800  30.0000   1.0000   1.0000 ; this_tempo: 40.0000, cur_metro: 1.5000, cur_step: 2.0000, 

```

realizziamo un [filtro di testo](./cs2pic.awk)
scritto in linguaggio `awk` per convertire
detta partitura in istruzioni per il pre-processore `pic`:

```awk
BEGIN {
   print ".PS\ncopy \"header.pic\""
}
END {
   print ".PE"
}
$1 == "i003" && $2 < 60 {
   at = $2;
   dur = $3;
   freq = $5;
   et = at+dur;
   printf("line from OuterFrame.sw + (%8.4f*onesecond, %8.4f*onehertz) to OuterFrame.sw + (%8.4f*onesecond, %8.4f*onehertz)\n", at, freq, et, freq);
}
```

Detto filtro trasforma la [partitura sopra esposta](./output_score.sco)
nel seguente insieme di istruzioni per il preprocessore `pic`:

```pic
.PS
copy "header.pic"
line from OuterFrame.sw + ( 20.0000*onesecond, 190.7400*onehertz) to OuterFrame.sw + ( 20.1500*onesecond, 190.7400*onehertz)
line from OuterFrame.sw + ( 20.0000*onesecond, 170.7400*onehertz) to OuterFrame.sw + ( 20.1500*onesecond, 170.7400*onehertz)
line from OuterFrame.sw + ( 20.5000*onesecond, 175.7400*onehertz) to OuterFrame.sw + ( 20.6500*onesecond, 175.7400*onehertz)
line from OuterFrame.sw + ( 21.0000*onesecond, 180.7400*onehertz) to OuterFrame.sw + ( 21.1500*onesecond, 180.7400*onehertz)
line from OuterFrame.sw + ( 21.0000*onesecond, 180.7400*onehertz) to OuterFrame.sw + ( 21.1500*onesecond, 180.7400*onehertz)
line from OuterFrame.sw + ( 21.3000*onesecond, 180.7400*onehertz) to OuterFrame.sw + ( 21.4500*onesecond, 180.7400*onehertz)
line from OuterFrame.sw + ( 21.3000*onesecond, 180.7400*onehertz) to OuterFrame.sw + ( 21.4500*onesecond, 180.7400*onehertz)
line from OuterFrame.sw + ( 21.5000*onesecond, 180.7400*onehertz) to OuterFrame.sw + ( 21.6500*onesecond, 180.7400*onehertz)
line from OuterFrame.sw + ( 21.5000*onesecond, 180.7400*onehertz) to OuterFrame.sw + ( 21.6500*onesecond, 180.7400*onehertz)
line from OuterFrame.sw + ( 22.2500*onesecond, 173.2400*onehertz) to OuterFrame.sw + ( 22.4000*onesecond, 173.2400*onehertz)
line from OuterFrame.sw + ( 50.0000*onesecond, 514.7300*onehertz) to OuterFrame.sw + ( 50.2000*onesecond, 514.7300*onehertz)
line from OuterFrame.sw + ( 50.0000*onesecond, 454.7300*onehertz) to OuterFrame.sw + ( 50.3000*onesecond, 454.7300*onehertz)
line from OuterFrame.sw + ( 50.3333*onesecond, 474.7300*onehertz) to OuterFrame.sw + ( 50.6333*onesecond, 474.7300*onehertz)
line from OuterFrame.sw + ( 50.5000*onesecond, 484.7300*onehertz) to OuterFrame.sw + ( 50.7000*onesecond, 484.7300*onehertz)
line from OuterFrame.sw + ( 50.5000*onesecond, 484.7300*onehertz) to OuterFrame.sw + ( 50.8000*onesecond, 484.7300*onehertz)
line from OuterFrame.sw + ( 50.7000*onesecond, 484.7300*onehertz) to OuterFrame.sw + ( 50.9000*onesecond, 484.7300*onehertz)
line from OuterFrame.sw + ( 50.7000*onesecond, 484.7300*onehertz) to OuterFrame.sw + ( 51.0000*onesecond, 484.7300*onehertz)
line from OuterFrame.sw + ( 51.0000*onesecond, 484.7300*onehertz) to OuterFrame.sw + ( 51.2000*onesecond, 484.7300*onehertz)
line from OuterFrame.sw + ( 51.0000*onesecond, 484.7300*onehertz) to OuterFrame.sw + ( 51.3000*onesecond, 484.7300*onehertz)
.PE
```

[`header.pic`](./header.pic) è:

```pic
.PS
scale=2.54;
maxpswid=29.7;
maxpsht=21;
totdur = 60;
framewid = 25;
onesecond = framewid/totdur;
maxfreq = 600;
frameht = 18;
onehertz = frameht/maxfreq;
print "onesecond: " onesecond ", onehertz: " onehertz
OuterFrame: box wid framewid ht frameht
.PE
```

```sh
$ awk -f cs2pic.awk output_file.sco | groff -p -P-l -Tpdf > output_file.pdf
```

![output_file.png](./output_file.png)
