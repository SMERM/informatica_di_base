BEGIN {
   print ".PS\ncopy \"header.pic\""
}
END {
   print ".PE"
}
$1 == "i003" && $2 < 60 {
   at = $2;
   dur = $3;
   freq = $5;
   et = at+dur;
   printf("line from OuterFrame.sw + (%8.4f*onesecond, %8.4f*onehertz) to OuterFrame.sw + (%8.4f*onesecond, %8.4f*onehertz)\n", at, freq, et, freq);
}
