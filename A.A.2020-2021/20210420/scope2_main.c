#include <stdio.h>

extern int a;

int sum(int b)
{
  return b+a;
}

void change_a(int b)
{
  a=b;
}

int main()
{
  printf("sum(%d)=%d\n", 6,sum(6));
  change_a(36);
  printf("sum(%d)=%d\n", 6,sum(6));
}
