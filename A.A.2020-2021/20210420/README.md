# Bernardini - Fondamenti di Informatica - Seminario del 20/04/2021

## [YouTube video](https://youtu.be/MWu5cg8MaDk)

## Programmazione

### Strutture e algoritmi

* scalari
  * tipi naturali ![tipi naturali](./tipi_naturali.png)
  * ambito operativo (`scope`) ![scoping](./scalars_and_scoping.png)
* vettori (*array*) ![vettori](./arrays.png)

## *Hands-on*

<!-- scalar.c scope2.c scope2_main.c scope3.c scope.c -->

### [Scalari](./scalar.c)

```C
int a = 23;
/* int if = 23;sbagliato
int 23a = 23;sbagliato*/
int _Giulio = 23;
int
giulio23 =
23;
```

Compilare con l'opzione `-c`:

```sh
$ cc -c scalar.c
```

### [Ambito 1 (variabili locali e volatili)](./scope.c)

```C
#include <stdio.h>


int fun(int a, int b)
{
  int c;
  c=a+b;
  return c;
}

int sum()
{
  int c=fun(17,6);
  return c+23;
}

int main()
{
  printf("fun(%d,%d)=%d\n", 17,6,fun(17,6));
  printf("sum()=%d\n", sum());
}
```

### [Ambito 2 (variabili globali o esterne)](./scope_2.c) e il suo [`main`](./scope2_main.c)

`scope2.c`:

```C
int a=17;
```

`scope2_main.c`:

```C
#include <stdio.h>

extern int a;

int sum(int b)
{
  return b+a;
}

void change_a(int b)
{
  a=b;
}

int main()
{
  printf("sum(%d)=%d\n", 6,sum(6));
  change_a(36);
  printf("sum(%d)=%d\n", 6,sum(6));
}
```

```sh
$ cc -o scope2 scope2.c scope2_main.c
$ ./scope2
sum(6)=23
sum(6)=42
```

### [Ambito 3 (variabili statiche o semi-globali)](./scope3.c) e il suo [`main`](./scope3_main.c)

```C
static int a=17;
int return_a()
{
  return a;
}
```

```C
#include <stdio.h>

int a=36;

extern int return_a();

int main()
{
  printf("a locale: %d\n", a);
  printf("a remoto: %d\n", return_a());
}
```

```sh
$ cc -o scope3 scope3.c scope3_main.c
$ ./scope3
a locale: 36
a remoto: 17
```

## Q&A sul *teorema di Shannon*

![Teorema di Shannon](./shannon.png)
