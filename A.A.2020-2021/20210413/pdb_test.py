
import pdb

def A(arg):
    return arg + 5

def B():
   v = A(25)
   print(v)

pdb.set_trace()
B()
