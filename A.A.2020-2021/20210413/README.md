# Bernardini - Fondamenti di Informatica - Seminario del 13/04/2021

## [YouTube video](https://youtu.be/VrdUzoXyY38)

## Programmazione

### Fondamenti

* riutilizzabilità del codice
  * librerie statiche
  * librerie dinamiche
* codice ibrido

![processo di compilazione](./compilazione.png)

### Strategie di programmazione

* Agile Programming
* Test-Driven Development
* Behaviour-Driven Development
* errori:
  * errori di sintassi
  * *compile-time* errors
  * *run-time* errors
  * *bugs*

![AP, TDD e BDD](./agile_tdd_bdd.png)

### Illustrazioni di errori

#### Errori di Sintassi

##### [in `C`](./error.c):

```C
void this_error(int a, int b
{
  // errore
}
```
    (manca la parentesi di chiusura degli argomenti della funzione)
    tentando di compilare:
```sh
$ cc error.c
error.c:4:1: error: expected ‘;’, ‘,’ or ‘)’ before ‘{’ token
 {
 ^
```
    (come si può notare, il compilatore si accorge della parentesi mancante
    alla **riga successiva**, poiché il `C` consente una formattazione libera)

##### [in `ruby`](./error.rb):

```ruby
def fun(arg, arg2
  # questo genera un errore
end
```
    (manca la parentesi di chiusura degli argomenti della funzione)
    tentando di interpretare:
```sh
$ ruby error.rb
error.rb:4: syntax error, unexpected keyword_end, expecting ')'
```
    (anche in questo caso, l'interprete si accorge dell'errore quando incontra
    la keyword `end`; non ha modo di accorgersene prima)

##### [in `python`](./error.py):
```python
def error(a, b):
        c = a
	print("this is a mistake")
```
    (questo caso è più complicato perché `python` considera errori anche le
    divergenze di allineamento tra le righe, quindi elementi grammaticali
    invisibili)
```sh
$ python error.py
  File "error.py", line 5
    print("this is a mistake")
                             ^
TabError: inconsistent use of tabs and spaces in indentation
```

### [Sessione di debugging in `python`](./pdb_test.py)

```python
import pdb

def A(arg):
    return arg + 5

def B():
   v = A(25)
   print(v)

pdb.set_trace()
B()
```

`pdb` è la libreria di debug di `python`. Importando `pdb` e inserendo la riga
`pdb.set_trace()` si può percorrere un programma riga per riga, come nella
sessione che segue:

```python
Script started on 2021-04-13 19:48:15+02:00 [TERM="xterm-256color" TTY="/dev/pts/11" COLUMNS="143" LINES="43"]
$ python pdb_test.py
> pdb_test.py(12)<module>()
-> B()
(Pdb) s
--Call--
> pdb_test.py(7)B()
-> def B():
(Pdb) n
> pdb_test.py(8)B()
-> v = A(25)
(Pdb) s
--Call--
> pdb_test.py(4)A()
-> def A(arg):
(Pdb) n
> pdb_test.py(5)A()
-> return arg + 5
(Pdb) p arg
25
(Pdb) n
--Return--
> pdb_test.py(5)A()->30
-> return arg + 5
(Pdb) n
> pdb_test.py(9)B()
-> print(v)
(Pdb) p v
30
(Pdb) n
30
--Return--
> pdb_test.py(9)B()->None
-> print(v)
(Pdb) n
--Return--
> pdb_test.py(12)<module>()->None
-> B()
(Pdb) n
nicb-p302u:.../20210413$ exit

Script done on 2021-04-13 19:49:02+02:00 [COMMAND_EXIT_CODE="0"]
```
    * i comandi `pdb` utilizzati in questa sessione sono:
      * `s` (`step`) procedi di riga in riga senza saltare niente
      * `n` procedi alla prossima istruzione saltando tutti i passi intermedi
      * `p <var>` stampa il valore della variabile `<var>`
      `pdb` ha anche un *help* in linea richiamabile con il comando `h`
