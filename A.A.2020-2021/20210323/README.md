# Bernardini - Fondamenti di Informatica - Seminario del 23/03/2021

[YouTube video](https://youtu.be/GZA4OvM0R2c)

### utilità di servizio

* sistemi di SCM (*source code management*): `git`, `svn`, `hg`, ...
  ![sistemi di SCM](./sistemi_scm.png)
  ![funzionamento](./scm_working.png)
* sistemi di CI (*continuous integration*): `travis`, `circle`, `jenkins`, ...
* *containers*: `docker`, `kubernetes`, ...
* *clouds*: `paas`, `saas`, `iaas`, ...

## Ecosistema di sviluppo

### Software Libero

* definizioni e distinguo
* sorgenti
* sistemi operativi
* sistemi di packaging

## Programmazione

![concetto di programmazione](./programmazione.png)

### Linguaggi

* compilatori e interpreti

#### [Un generatore random interpretato (`python`)](./compilatori_e_interpreti/myrand.py)

```python
from random import random

while(True):
    print(random())
```

Eseguendolo *direttamente* da linea di comando otterremo:

```sh
$ python myrand.py
0.171064117678
0.690754081582
0.101073776394
0.811048187063
.... (ad libitum)

```

(per interrompere il programma digitare Ctrl-C)


#### [Un generatore random compilato (`C`)](./compilatori_e_interpreti/random.c)

```C
#include <stdlib.h>
#include <stdio.h>

int main()
{
    while(1)
      printf("%d\n", rand());
}
```

In questo caso prima di eseguire il programma è necessario *compilarlo*, ossia
invocare il compilatore `C` (`cc`) e trasformare il *codice sorgente* in
*codice macchina*, così:

```sh
$ cc -o ./random random.c
```

A questo punto è possibile eseguire *direttamente* il programma compilato:

```sh
$ ./random
1804289383
846930886
1681692777
1714636915
... (ad libitum)
```

(per interrompere il programma digitare Ctrl-C)
