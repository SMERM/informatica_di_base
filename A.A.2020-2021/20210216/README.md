# Bernardini - Fondamenti di Informatica - Seminario del 16/02/2021

## [YouTube video](https://youtu.be/GG1JMmJ4fA4)

### Emulazione di terminale

### `bash` shell

* *command-line completion*
* bash globbing
* variabili di *environment*
  * inizializzazione
  * richiamo
* funzioni interne alla `bash shell`:
  * funzioni di controllo:
    * `if`-`then`-`else`-`fi`
    * `for`-`do`-`done`
    * `while`-`do`-`done`
* *scripts* eseguibili

### comandi principali

* struttura generale dei comandi (comandi, opzioni, argomenti)
* `cd` (nomenclatura)
* `ls`
* `echo`
* `cat`
* `man`
* `apropos`
* `more`/`less`
* `chmod`
* `chown`

# Appunti di @DavideTedesco

## Shell

Comandi testuali alla macchina, essi sono delle scritte per far partire qualche programma.

### `ls`

Lista degli elementi all'interno della cartella in cui si è.

### `cd`

Per cambiare directory

### `pwd`

Comando per dire dove siamo nel disco.

### Come sono fatti i comandi?

I comandi sono dei programmi veri e propri.

```sh
ls -l /bin/ls
```

Dove è collocato `ls` nel disco?

La shell bash è una shell moderna ed ha delle cose che aiutano la digitazione, ovvero l'autocompletion.

### Autocompletion

Quando si digita un comando, sia nel caso dei comandi sia nel caso di *file*
presenti nella cartella corrente, premendo il tasto `tab` (`-->|`) si dà luogo
all'auto-completamento del comando.

L'auto-completamento funziona soltanto in  caso  vi  sia  un  file  con  quel  nome  con
l'iniziale, il completamento viene fatto solamente su  file  o  cartelle
esistenti.

In caso di ambiguità, la shell emette un suono e aspetta la disambiguazione da
parte dell'utente. Premendo più volte il tasto `tab` la shell elenca tutte le
possibilit\`a disponibili.

### Come si fa a conoscere tutti i comandi?

`aprops <argomento>`

## Vari modelli di documentazione interna

### Comandi per documentazione

* `man`: documentazione di riferimento di un comando
* `info`: documentazione esaustiva di strutture complesse (ad es. `latex`, `emacs`, ecc.)

## Comandi

* `echo`
* `cat`

## Cosa può fare la shell?

* comando `while`
* comando `for`
* `less`
* `more`
* `ps` -> lista processi attivi su questa macchina

La shell ha la possibilità di avere delle variabili...
PID = Process ID

## Interrupts

Nella programmazione del software si possono inviare e ricevere segnali:

* `kill`: invia un segnale a un dato PID
* `trap`: (dentro al software) raccoglie il segnale inviato

```man
Some of the more commonly used signals:

     1       HUP (hang up)
     2       INT (interrupt)
     3       QUIT (quit)
     6       ABRT (abort)
     9       KILL (non-catchable, non-ignorable kill)
     14      ALRM (alarm clock)
     15      TERM (software termination signal)
```

Il software fatto bene fa un `trap` di tutti i segnali, e in base ai segnali
ricevuti decide il da farsi. Ad es. (esempio stupido):

```sh
$ echo $BASHPID
22071
$ trap "echo ciao!" 3
$ kill -3 $BASHPID
ciao!
$
```

## Piccoli comandi con la shell

Con la shell si possono scrivere piccoli programmi, questo file.

## Variabili d'ambiente predefinite

* `echo $PS1`: prompt dei comandi (riconfigurabile)
______

[Documentazione Shell Bash Online](https://www.tutorialspoint.com/execute_bash_online.php)

## Terminal history

```sh
$ history
  519  cd README.md
  520  ls
  521  ls -l /bin/ls
  522  ls --version
  523  ls --help
  524  ls -h
  525  man ls
  526  ls -v
  527  ls --v
  528  ls --version
  529  sudo ls --version
  530  mkdir new_folder
  531  ls
  532  ls new_folder/
  533  ls README.md
  534  apropos file
  535  clear
  536  man latex
  537  info latex
  538  info latex2e
  539  man faust
  540  clear
  541  pwd
  542  l
  543  ls
  544  cd new_folder/
  545  ls
  546  echo ciao
  547  x = 0 while x -lt 1000 do
  548  x = 0 while x -lt 1000 do
  549  echo $x
  550  x=0 while [ x -lt 1000 ] do
  551  x=0; while [ x -lt 1000 ] do ; n=$(printf "%03d" $x); echo file$n >file$n; done
  552  ls
  553  x=0; while [ $x -lt 1000 ] do ; n=$(printf "%03d" $x); echo file$n >file$n; let x=$x+1 ;  done
  554  x=0; while [ $x -lt 1000 ]; do  n=$(printf "%03d" $x); echo file$n >file$n; let x=$x+1 ;  done
  555  ls
  556  ls -1 | wc -l
  557  man wc
  558  wc ../README.md
  559  man wc
  560  for f in file*; do  newname=$(echo $f | sed "s/file/fuffi/"); mv $f $newname; done
  561  ls
  562  cat fuffi599
  563  cat ../README.md
  564  cat ../README.md | less
  565  ls
  566  rm fuffi*
  567  ls
  568  echo ciao > ciao.txt
  569  ls
  570  rm -i ciao.txt
  571  ls
  572  rm -i ciao.txt
  573  ls
  574  variabile=23
  575  set
  576  echo $variabile
  577  bash
  578  export variabile
  579  bash
  580  echo $fuffi
  581  ps
  582  bash
  583  ps
  584  atom ciao5
  585  ls
  586  ls
  587  chmod +x ciao5
  588  ./ciao5
```
