# Bernardini - Fondamenti di Informatica - Seminario del 02/03/2021

[YouTube video](https://youtu.be/wlusxcir8Gs)

### `bash` shell

* scripting: *hash bang* (`#!`)
* scripts letti all'inizio di una shell: `.profile`, `.bashrc`
* `alias`
* operatori
* espressioni regolari
  * nei linguaggi: `awk`, `perl`, `ruby`, `python`, ...
* `grep`
* `sed`
* `awk`
* `make`

### utilità audio cli

* `sox`
* `lame`
* `ffmpeg`

## Esercizi svolti in classe

### [*Shell scripting*](./dur)

* Realizzare uno script che totalizza le durate di file .wav
  dati in argomento. Le durate vengono estrapolate dal comando
  `sndinfo` preso dalla suite `csound`:
```bash
#!/bin/bash


if [ $# -lt 2 ]
then
  echo "usage: $0 file [...]" 1>&2
  exit -1
fi

sndinfo $@ |& grep seconds| sed -e "s/^\s*//"| cut -d" " -f7| awk 'BEGIN {totale=0}{totale+=$1}END{print totale}'
exit 0
```

### [Esempio di `Makefile`](./Makefile

```make
dur.output: dur Makefile
	./dur suoni/*.wav>dur.output

clean:
	rm dur.output
```

## [registrazione del terminale](./fdi_20210302.txt)

## Esercizio a casa (scadenza: 08/03/2021)

* modificare lo [*shell script*](./dur) [`dur`](./dur) per
  elencare anche i nomi dei file e le durate di ciascuno
  (oltre al totale già presente). Un esempio di output
  potrebbe essere:
```
xxx.wav:       14.023
yyy.wav:        3.004

totale:        17.027
```
