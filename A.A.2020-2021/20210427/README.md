# Bernardini - Fondamenti di Informatica - Seminario del 27/04/2021

## [YouTube video](https://youtu.be/dcV7x6hn1XU) (solo audio)

## Programmazione

### Strutture e algoritmi

* puntatori
  ![puntatori](./pointers.png)
* strutture dati

### Codice `C` realizzato durante la lezione

#### Puntatori

##### [`punt0.c`](./punt0.c)

```C
#include <stdio.h>

void main()
{
	char a = 23;
	char *p = &a;
	printf("a: %d,\tindirizzo di a: 0x%p,\tvalore puntatore: 0x%p,\tdereferenziato: %d\n",a,&a,p,*p);


}
```

```sh
$ cc -o punt0 punt0.c
$ ./punt0
a: 23,	indirizzo di a: 0x0x7fff76742667,	valore puntatore: 0x0x7fff76742667,	dereferenziato: 23
```

##### [`punt1.c`](./punt1.c)

```C
#include <stdio.h>

void main()
{
	char a[] ={1,2,3,4};
	char *p = a;
	printf("a: %d,\tindirizzo di a: %p,\tvalore puntatore: %p,\tdereferenziato: %d\n",a[0],a,p,*p);
	p=p+1;
	printf("a: %d,\tindirizzo di a: %p,\tvalore puntatore: %p,\tdereferenziato: %d\n",a[1],a,p,*p);
}
```

```sh
$ cc -o punt1 punt1.c
$ ./punt1
a: 1,	indirizzo di a: 0x7fff21a82324,	valore puntatore: 0x7fff21a82324,	dereferenziato: 1
a: 2,	indirizzo di a: 0x7fff21a82324,	valore puntatore: 0x7fff21a82325,	dereferenziato: 2
```

##### [`punt2.c`](./punt2.c)

```C
#include <stdio.h>

void main()
{
	double a[] ={1,2,3,4};
	double *p = a;
	printf("a: %f,\tindirizzo di a: %p,\tvalore puntatore: %p,\tdereferenziato: %f\n",a[0],a,p,*p);
	p=p+1;
	printf("a: %f,\tindirizzo di a: %p,\tvalore puntatore: %p,\tdereferenziato: %f\n",a[1],a,p,*p);
}
```

```sh
$ cc -o punt2 punt2.c
$ ./punt2
a: 1.000000,	indirizzo di a: 0x7ffee1a4c120,	valore puntatore: 0x7ffee1a4c120,	dereferenziato: 1.000000
a: 2.000000,	indirizzo di a: 0x7ffee1a4c120,	valore puntatore: 0x7ffee1a4c128,	dereferenziato: 2.000000
```

##### [`punt3.c`](./punt3.c)

```C
#include <stdio.h>

void main()
{
	char a[] ={1,2,3,4};
	char *p = a;
	printf("a[0]: %d, a[1]: %d, a[2]: %d\n",*p,*(p+1),*(p+2));
}
```

```sh
$ cc -o punt3 punt3.c
$ ./punt3
a[0]: 1, a[1]: 2, a[2]: 3
```

##### [`punt4.c`](./punt4.c)

```C
#include <stdio.h>

void a()
{
	printf("sono la funzione a\n");
}

void b()
{
	printf("sono la funzione b\n");
}


void main()
{
	/*dichiarazione inizializzata*/
	void (*p)()=a;
	void (*pa[])()={a,b};
	/*dereferenziazione*/
	(*p)();
	(*(pa+1))();
}
```

```sh
$ cc -o punt4 punt4.c
$t ./punt4
sono la funzione a
sono la funzione b
```

#### Strutture dati

##### [`struct1.c`](./struct1.c)

```C
#include <stdio.h>
#include <string.h>

struct Nota{

	double at;
	double dur;
	double freq;
	double amp;
	char nome[128];
};

struct Nota*init_Nota(struct Nota*p,double at,double dur,double freq,double amp,char nome[])
{

	(*p).at= at;
	p->dur= dur;
	p->freq= freq;
	p->amp= amp;
	strcpy(p->nome,nome);
	return p;
}

void main(){
	struct Nota n;
	struct Nota *p = &n;
	init_Nota(p,0,2,40,-6,"eidjei");
	printf("at: %f,dur: %f,freq: %f,amp: %f,nome: %s\n", n.at, n.dur, n.freq, n.amp, n.nome);
	printf("una nota è grande: %ldbytes\n",sizeof(struct Nota));

}
```

```sh
$ cc -o struct1 struct1.c
$ ./struct1
at: 0.000000,dur: 2.000000,freq: 40.000000,amp: -6.000000,nome: eidjei
una nota è grande: 160bytes
```

##### [`struct2.c`](./struct2.c)

```C
#include <stdio.h>
#include <string.h>

struct Nota{

	double at;
	double dur;
	double freq;
	double amp;
	char nome[128];
};

struct Nota*init_Nota(struct Nota*p,double at,double dur,double freq,double amp,char nome[])
{

	(*p).at= at;
	p->dur= dur;
	p->freq= freq;
	p->amp= amp;
	strcpy(p->nome,nome);
	return p;
}

void main(){


	struct Nota n[3];
	struct Nota *p = n;
	init_Nota(p,0,2,40,-6,"eidjei");
	init_Nota(p+1,3,2,440,-6,"eidjei");
	init_Nota(p+2,5,2,840,-6,"eidjei");
	printf("&n[0]: %p\n",p);
	printf("&n[1]: %p\n",p+1);
	printf("&n[2]: %p\n",p+2);
 
}


```

```sh
$ cc -o struct2 struct2.c
$ ./struct2
&n[0]: 0x7ffde24cb860
&n[1]: 0x7ffde24cb900
&n[2]: 0x7ffde24cb9a0
```
