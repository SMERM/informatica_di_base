#include <stdio.h>
#include <string.h>

struct Nota{

	double at;
	double dur;
	double freq;
	double amp;
	char nome[128];
};

struct Nota*init_Nota(struct Nota*p,double at,double dur,double freq,double amp,char nome[])
{

	(*p).at= at;
	p->dur= dur;
	p->freq= freq;
	p->amp= amp;
	strcpy(p->nome,nome);
	return p;
}

void main(){


	struct Nota n;
	struct Nota *p = &n;
	init_Nota(p,0,2,40,-6,"eidjei");
	printf("at: %f,dur: %f,freq: %f,amp: %f,nome: %s\n", n.at, n.dur, n.freq, n.amp, n.nome);
	printf("una nota è grande: %ldbytes\n",sizeof(struct Nota));

}

