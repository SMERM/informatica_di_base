#include <stdio.h>
#include <string.h>

struct Nota{

	double at;
	double dur;
	double freq;
	double amp;
	char nome[128];
};

struct Nota*init_Nota(struct Nota*p,double at,double dur,double freq,double amp,char nome[])
{

	(*p).at= at;
	p->dur= dur;
	p->freq= freq;
	p->amp= amp;
	strcpy(p->nome,nome);
	return p;
}

void main(){


	struct Nota n[3];
	struct Nota *p = n;
	init_Nota(p,0,2,40,-6,"eidjei");
	init_Nota(p+1,3,2,440,-6,"eidjei");
	init_Nota(p+2,5,2,840,-6,"eidjei");
	printf("&n[0]: %p\n",p);
	printf("&n[1]: %p\n",p+1);
	printf("&n[2]: %p\n",p+2);
 
}


