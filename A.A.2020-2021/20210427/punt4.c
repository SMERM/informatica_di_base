#include <stdio.h>

void a()
{
	printf("sono la funzione a\n");
}

void b()
{
	printf("sono la funzione b\n");
}


void main()
{
	/*dichiarazione inizializzata*/
	void (*p)()=a;
	void (*pa[])()={a,b};
	/*dereferenziazione*/
	(*p)();
	(*(pa+1))();
}



