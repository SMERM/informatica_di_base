# Bernardini - Fondamenti di Informatica - Seminario del 09/02/2021

## [YouTube video](https://youtu.be/vCoFQOgiLp0)

## Fondamenti di macchine di calcolo

* schema generale di funzionamento
  * standards: *ASCII*, *UTF-8*, *RGB*, ecc.
  * sequenza di boot
    * ![sequenza di boot](./lavagna_sequenza_di_boot.jpg)

### Software

* sistema operativo:
  * gestione centralizzata delle risorse
  * SO monolitici vs. micro-kernel
    * ![kernels and drivers](./kernel_drivers.png)
  * *time-sharing* operation:
    * `user space` vs `kernel space`
    * `tasks`
      * `sleep`
    * `threads`
      * *semafori*
      * `mutex`
* `shell`
  * ![sistema operativo e `shell`](./so_shell.png)
  * command-line interface:
    * metafora della macchina da scrivere
  * GUI:
    * metafora del *desktop*
* chiamate sistema

### CLI vs. IDE

### *divide et impera* (*small is beautiful*)

* *everything is a file*
* I/O: stdin, stdout, stderr
* valori di ritorno dei comandi: `0` (nessun problema), `~0` (errore)
* *piping* e *redirections*
  * comandi logici: `&&`, `||`
  * ridirezioni: '<', '>', '2>&1', ecc.

