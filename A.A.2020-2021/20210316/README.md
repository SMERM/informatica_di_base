# Bernardini - Fondamenti di Informatica - Seminario del 16/03/2021

[YouTube video](https://youtu.be/Yn3Dqzt_F4Y)

## Reti

* Modello client-server
  * esempio semplificativo e spiegato
  * il comando `nc`
  * Protocolli (con esempi: `HTTP`, `DNS`, `DHCP`...)
* Architettura multi-tier
  * separazione degli strati
    * three-tier: presentation, logic, data storage
    * modello MVC (model-view-controller)
  * Esempi:
    * applicazioni web - con estensioni *app*

### Multi-tier architecture

![Multi-tier architecture](./multi_tier_architecture.png)

### Virtual Machines e cloud

![VM e cloud](./vms_and_clouds.png)
