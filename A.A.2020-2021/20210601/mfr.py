from os import error
import struct
class MidiFileHeader:
    def __init__(self,fh):
         fh.seek(0)
         magic=fh.read(4)
         magics=magic.decode("ascii")  
         if (magics!="MThd"):
             raise Exception("this is not a midi file you dumbass!")
         hs=struct.unpack(">L",fh.read(4))    
         self.headersize=hs[0]
         hhs=struct.unpack('>HHH',fh.read(6))
         (self.format,self.tracks,self.divison)=hhs
         
    def print(self):
        print("size: %d,format: %d, ntracks: %d, division: %d"%(self.headersize, self.format,self.tracks,self.divison))
    
    def next(self):
        return self.headersize

class MFR:
    def __init__(self,mf):
        self.midifile=mf
        self.setup()
    
    def setup(self):
        with open(self.midifile,"rb") as fh:
            self.read_header(fh)
            self.read_tracks(fh)
    
    def read_header(self,fh):
        self.header=MidiFileHeader(fh)
    
    
    def read_tracks(self,fh):
        pass  




#