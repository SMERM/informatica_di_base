# Bernardini - Fondamenti di Informatica - Seminario del 01/06/2021

## [YouTube video](https://youtu.be/95aBDQ4ib-Q)

## Programmazione

### Come si leggono files binari in formato `RIFF` e derivati

### [Oggetti che leggono un file `MIDI` (inizio)](./mfr.py)

```python
from os import error
import struct
class MidiFileHeader:
    def __init__(self,fh):
         fh.seek(0)
         magic=fh.read(4)
         magics=magic.decode("ascii")  
         if (magics!="MThd"):
             raise Exception("this is not a midi file you dumbass!")
         hs=struct.unpack(">L",fh.read(4))    
         self.headersize=hs[0]
         hhs=struct.unpack('>HHH',fh.read(6))
         (self.format,self.tracks,self.divison)=hhs
         
    def print(self):
        print("size: %d,format: %d, ntracks: %d, division: %d"%(self.headersize, self.format,self.tracks,self.divison))
    
    def next(self):
        return self.headersize

class MFR:
    def __init__(self,mf):
        self.midifile=mf
        self.setup()
    
    def setup(self):
        with open(self.midifile,"rb") as fh:
            self.read_header(fh)
            self.read_tracks(fh)
    
    def read_header(self,fh):
        self.header=MidiFileHeader(fh)
    
    
    def read_tracks(self,fh):
        pass  

```

Questi oggetti possono essere provati col [codice seguente](./testone.py):

```python
from mfr import MFR

mf=MFR("untitled.mid")

mf.header.print()
```

```sh
$ python3 testone.py
size: 6,format: 1, ntracks: 2, division: 96
```
