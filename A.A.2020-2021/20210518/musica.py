class Evento:
    def __init__(self,at,dur):
        self.at=at
        self.dur=dur

    def stampati(self):
        print("at: %.4f dur: %.4f\n"%(self.at,self.dur))

class Nota(Evento):
    def __init__(self,at,dur,amp,freq):
        super().__init__(at,dur)
        self.amp=amp
        self.freq=freq

    def stampati(self):
        super().stampati()
        print("amp: %.4f freq: %.4f\n"%(self.amp,self.freq))

n=Nota(0,2,-4,440)
n.stampati()
