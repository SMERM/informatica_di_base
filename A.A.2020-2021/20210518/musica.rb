class Evento
  attr_reader :at,:dur
  def initialize(at,dur)
    @at=at
    @dur=dur
  end
  def stampati()
    printf("at: %.4f, dur: %.4f\n",self.at,self.dur)
  end
end

class Nota<Evento
  attr_reader :amp,:freq
  def initialize(at,dur,amp,freq)
    super(at,dur)
    @amp=amp
    @freq=freq
  end
  def stampati()
    super()
    printf("amp: %.4f, freq: %.4f",self.amp,self.freq)
  end
end

n=Nota.new(0,2,-6,440)
n.stampati()
