#include <iostream>
class Evento
{

	double _at;
	double _dur;
public: 
	Evento(double at, double dur)
	{
		this->_at=at;
		this->_dur=dur;
	}
	inline double at(){return this->_at;}
	inline double dur(){return this->_dur;}
	virtual void stampati()
	{std::cout<< "at: "<< _at<<", dur: "<< _dur<<std::endl;}
};

class Nota: public Evento
{

	double _freq;
	double _amp;
public:
	Nota(double at,double dur, double freq, double amp): Evento(at,dur)
	{
	
		this->_freq=freq;
		this->_amp=amp;

	}
	virtual void stampati()
	{this->Evento::stampati();std::cout<<"freq: "<<_freq<<", amp: "<<_amp<<std::endl;}
};


int main()
{

	Nota *n= new Nota(0,2,440,-6);
	n->stampati();
}
