# Bernardini - Fondamenti di Informatica - Seminario del 18/05/2021

## [YouTube video](https://youtu.be/mucZQkZ8Z44)

## Programmazione

### *Object-Oriented Programming*: fondamenti

![OOP](./oop.png

#### [OOP in `C++`](./musica.cpp)

```C++
#include <iostream>
class Evento
{

	double _at;
	double _dur;
public: 
	Evento(double at, double dur)
	{
		this->_at=at;
		this->_dur=dur;
	}
	inline double at(){return this->_at;}
	inline double dur(){return this->_dur;}
	virtual void stampati()
	{std::cout<< "at: "<< _at<<", dur: "<< _dur<<std::endl;}
};

class Nota: public Evento
{

	double _freq;
	double _amp;
public:
	Nota(double at,double dur, double freq, double amp): Evento(at,dur)
	{
	
		this->_freq=freq;
		this->_amp=amp;

	}
	virtual void stampati()
	{this->Evento::stampati();std::cout<<"freq: "<<_freq<<", amp: "<<_amp<<std::endl;}
};


int main()
{

	Nota *n= new Nota(0,2,440,-6);
	n->stampati();
}
```

```sh
$ g++ -o ./musica musica.cpp
$ ./musica
at: 0, dur: 2
freq: 440, amp: -6
```

#### [OOP in `C`](./musica.c)

```C
#include <stdio.h>
#include <stdlib.h>

typedef struct evento
{
	
	double _at;
	double _dur;
	void (*_stampati)(struct evento *);
}Evento;

void stampatie(Evento* p)
{

	printf("at: %8.4f dur: %8.4f\n", p->_at, p->_dur);
}

Evento* Evento_(double at,double dur)
{

	Evento* result=(Evento *)malloc(sizeof(Evento));
	result->_at=at;
	result->_dur=dur;
	result->_stampati=stampatie;
	return result;
}

typedef struct nota
{

	Evento *e; /* l'ereditarietà funziona solo se questo è il *primo* elemento */
	double _freq;
	double _amp;

}Nota;

void stampatin(Evento* p)
{
	Nota*np= (Nota *)p;
	stampatie(np->e);
	printf("freq: %8.4f amp: %8.4f\n",np->_freq, np->_amp);
}

Nota* Nota_(double at,double dur,double freq, double amp)
{

	Nota* result=(Nota *)malloc(sizeof(Nota));
	result->e=Evento_(at,dur);
	result->_freq=freq;
	result->_amp=amp;
	result->e->_stampati=stampatin;
	return result;
}

int main()
{

	Nota*n=Nota_(0,20,440,-6);
	(*(n->e->_stampati))((Evento*)n);
}
```

```sh
$ cc -o ./musica musica.c
$ ./musica
at:   0.0000 dur:  20.0000
freq: 440.0000 amp:  -6.0000
```

#### [OOP in `python`](./musica.c)

```python
class Evento:
    def __init__(self,at,dur):
        self.at=at
        self.dur=dur

    def stampati(self):
        print("at: %.4f dur: %.4f\n"%(self.at,self.dur))

class Nota(Evento):
    def __init__(self,at,dur,amp,freq):
        super().__init__(at,dur)
        self.amp=amp
        self.freq=freq

    def stampati(self):
        super().stampati()
        print("amp: %.4f freq: %.4f\n"%(self.amp,self.freq))

n=Nota(0,2,-4,440)
n.stampati()
```

```sh
$ python3 musica.py
at: 0.0000 dur: 2.0000

amp: -4.0000 freq: 440.0000
```
#### [OOP in `ruby`](./musica.rb)

```ruby
class Evento
  attr_reader :at,:dur
  def initialize(at,dur)
    @at=at
    @dur=dur
  end
  def stampati()
    printf("at: %.4f, dur: %.4f\n",self.at,self.dur)
  end
end

class Nota<Evento
  attr_reader :amp,:freq
  def initialize(at,dur,amp,freq)
    super(at,dur)
    @amp=amp
    @freq=freq
  end
  def stampati()
    super()
    printf("amp: %.4f, freq: %.4f",self.amp,self.freq)
  end
end

n=Nota.new(0,2,-6,440)
n.stampati()
```

```sh
$ ruby musica.rb
at: 0.0000, dur: 2.0000
amp: -6.0000, freq: 440.0000
```
