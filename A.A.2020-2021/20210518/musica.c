#include <stdio.h>
#include <stdlib.h>

typedef struct evento
{
	
	double _at;
	double _dur;
	void (*_stampati)(struct evento *);
}Evento;

void stampatie(Evento* p)
{

	printf("at: %8.4f dur: %8.4f\n", p->_at, p->_dur);
}

Evento* Evento_(double at,double dur)
{

	Evento* result=(Evento *)malloc(sizeof(Evento));
	result->_at=at;
	result->_dur=dur;
	result->_stampati=stampatie;
	return result;
}

typedef struct nota
{

	Evento *e;
	double _freq;
	double _amp;

}Nota;

void stampatin(Evento* p)
{
	Nota*np= (Nota *)p;
	stampatie(np->e);
	printf("freq: %8.4f amp: %8.4f\n",np->_freq, np->_amp);
}

Nota* Nota_(double at,double dur,double freq, double amp)
{

	Nota* result=(Nota *)malloc(sizeof(Nota));
	result->e=Evento_(at,dur);
	result->_freq=freq;
	result->_amp=amp;
	result->e->_stampati=stampatin;
	return result;
}

int main()
{

	Nota*n=Nota_(0,20,440,-6);
	(*(n->e->_stampati))((Evento*)n);
}
