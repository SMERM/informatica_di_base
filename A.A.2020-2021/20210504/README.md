# Bernardini - Fondamenti di Informatica - Seminario del 04/05/2021

## [YouTube video](https://youtu.be/YwuG7yfxfVA)

## Programmazione

### Runtime faults & `core dump`s

Realizzazione di un [`segmentation fault` (voluto)](./seg_fault.c):

```C
#include <stdio.h>

int main()
{
	int *p=(int *)0xffffff;
	int x=*p;
	printf("x=%d\n", x);
}
```

L'indirizzo `0xffffff` al quale punta il puntatore `int *p` è fuori dallo
spazio di memoria di questo programma, quindi questo programma genera un
`segmentation fault`.

```sh
$ cc -o seg_fault seg_fault.c
./seg_fault
Segmentation fault      ( ./seg_fault )
```

### Ripasso del comando `make` e dei `Makefile`s

Realizzazione di un [`Makefile` tipico](./Makefile) con anche sotto-directories:

```Makefile
TARGET=seg_fault
SOURCE=seg_fault.c
CC=cc

$(TARGET): $(SOURCE)
	$(CC) -o $(TARGET) $(SOURCE)

clean:
	$(RM) $(TARGET) *ciao

ciao: pre.ciao
	cat pre.ciao|sed -e "s/^pre\./see.vabbe/">ciao
pre.ciao: pre.pre.ciao
	cat pre.pre.ciao|sed -e "s/^pre\.//">pre.ciao
pre.pre.ciao:
	echo pre.pre.ciao>pre.pre.ciao

subdir:
	$(MAKE) -C ./subdir -$(MAKEFLAGS)

.PHONY: subdir
```

```sh
$ make
cc -o seg_fault seg_fault.c
$ make ciao
echo pre.pre.ciao>pre.pre.ciao
cat pre.pre.ciao|sed -e "s/^pre\.//">pre.ciao
cat pre.ciao|sed -e "s/^pre\./see.vabbe/">ciao
$ make clean
rm -f seg_fault *ciao
$ make subdir
make -C ./subdir -
make[1]: Entering directory './20210504/subdir'
echo pre.pre.ciao>pre.pre.ciao
cat pre.pre.ciao|sed -e "s/^pre\.//">pre.ciao
cat pre.ciao|sed -e "s/^pre\./see.vabbe/">ciao
make[1]: Leaving directory './20210504/subdir'
```

### Strutture e algoritmi

* *array* associativi
* un [*array associativo* in `python` (`dict`)](./hash.py):
```python
a={"ciao": "hello", "si vediamo": "bye"}
print (a["ciao"])
print (a["si vediamo"])
```

```sh
$ python hash.py
hello
bye
```
* un [*array associativo* in `C`](./hash.c):
```C
#include <stdio.h>
#include <string.h>
typedef struct
{
	char key[256];
	char value[256];
}Element;

Element *element_create(Element *e,char k[],char v[])
{
	strcpy (e->key,k);
	strcpy (e->value,v);
	return e;
}

char *element_lookup(Element ea[],int size,const char k[],char risultato[])
{
	Element *p=ea;
	while (size-->0)
	{
		if (strcmp(p->key,k)==0)
		{
			strcpy(risultato,p->value);
			break;
		}
		++p;
	}
	return risultato;
}

int main()
{
	Element dict[3];
	char ris[256];
	element_create(&dict[0],"ciao","hello");
	element_create(&dict[1],"si vediamo","bye");
	element_create(&dict[2],"vabbe basta","please stop");
	printf("%s ->%s\n","ciao",element_lookup(dict,3,"ciao",ris));
	printf("%s ->%s\n","si vediamo",element_lookup(dict,3,"si vediamo",ris));
	printf("%s ->%s\n","vabbe basta",element_lookup(dict,3,"vabbe basta",ris));

}
```

```sh
$ cc -o hash hash.c
ciao ->hello
si vediamo ->bye
vabbe basta ->please stop
```
