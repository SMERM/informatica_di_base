#include <stdio.h>
#include <string.h>
typedef struct
{
	char key[256];
	char value[256];
}Element;

Element *element_create(Element *e,char k[],char v[])
{
	strcpy (e->key,k);
	strcpy (e->value,v);
	return e;
}

char *element_lookup(Element ea[],int size,const char k[],char risultato[])
{
	Element *p=ea;
	while (size-->0)
	{
		if (strcmp(p->key,k)==0)
		{
			strcpy(risultato,p->value);
			break;
		}
		++p;
	}
	return risultato;
}

int main()
{
	Element dict[3];
	char ris[256];
	element_create(&dict[0],"ciao","hello");
	element_create(&dict[1],"si vediamo","bye");
	element_create(&dict[2],"vabbe basta","please stop");
	printf("%s ->%s\n","ciao",element_lookup(dict,3,"ciao",ris));
	printf("%s ->%s\n","si vediamo",element_lookup(dict,3,"si vediamo",ris));
	printf("%s ->%s\n","vabbe basta",element_lookup(dict,3,"vabbe basta",ris));

}
