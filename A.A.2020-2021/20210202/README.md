# Bernardini - Fondamenti di Informatica - Seminario del 02/02/2021

## [YouTube video](https://youtu.be/SoqqKkceDTA)

## Fondamenti di macchine di calcolo

* Prodromi: Leibnitz, Babbage
* schema generale di funzionamento
  * macchina di Turing
  * risolvibilità dei problemi
  * sistema numerico
    ![bits](./bits.png)

### Hardware

* componenti
  * ram
    ![ram](./ram.png)
    ![addressing](./addressing.png)
  * cpu
  * bus
  * storage
  * I/O

#### Struttura di un computer

![struttura di un computer](./lavagna_struttura_computer.jpg)

#### Cosa succede quando si preme un tasto sulla tastiera

![press a key](./lavagna_press_a_key.jpg)
