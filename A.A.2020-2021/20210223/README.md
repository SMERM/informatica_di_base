# Bernardini - Fondamenti di Informatica - Seminario del 23/02/2021

## [YouTube video](https://youtu.be/gFzhE1jeZgg)

## `bash` shell

* variabili speciali:
  * `$PATH` -> elenco di cartelle dei comandi
  * `$PS`* -> prompts
  * `$?` -> valore di ritorno
  * `$@` -> argomenti di un comando
  * `$#` -> numero degli argomenti
  * `$$` -> PID del comando
  * [riferimento](https://www.shell-tips.com/bash/environment-variables/)
* operatori
* scripting: *hash bang* (`#!`)

## Introduzione ai *file system*

L'organizzazione dei dischi:

* *File Allocation Table* (*FAT*)
* nomi dei files, *i-nodes*
* *clusters*

![FAT e clusters](./FAT_cluster.png)

## Realizzazione di un `shell script`

Realizzazione di un [`shell script`](./daje) per calcolare la durata totale di files `.wav`

```bash
#!/bin/bash

#
# Per utilizzare questo comando è necessario aver installato csound sul
# proprio computer (sndinfo è un comando della csound suite)
#

if [ $# -lt 2 ]
then
  echo "usage: $0 file [...]" 1>&2
  exit -1
fi

sndinfo $@ |& grep seconds | sed -e 's/^\s*//' | cut -d ' ' -f 7 | awk 'BEGIN { total = 0 } { total += $1 } END { print total }'
```
