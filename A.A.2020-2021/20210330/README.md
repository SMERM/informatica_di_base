# Bernardini - Fondamenti di Informatica - Seminario del 30/03/2021

## [YouTube video](https://youtu.be/vV2aNX5aVzA)

## Programmazione

### Linguaggi

* compilatori e interpreti
* ibridi (java, scala, python, jython, jruby, ecc.)

### Fondamenti

* perché esistono i linguaggi di programmazione?
* programmazione strutturata
* programmazione a oggetti

### Esempi

#### Programmazione strutturata

* Suddivisione fittizia del [programma realizzato in linguaggio `C` la
  settimana scorsa](../20210323/compilatori_e_interpreti/random.c) in
  due funzioni separate (disposte in due files separati):
  [file n.1 (`random_miomao.c`)](./random_miomao.c)
```C
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

void miomao()
{
    while(1)
      printf("%8.4f\n", cos(rand()));
}
```
   e [file n.2 (`random_main.c`)](./random_main.c)
```C
extern void miomao();

void main()
{
    miomao();
}
```

Poiché abbiamo introdotto la funzione `cos()`, che è contenuta nella libreria
matematica `libm`, dobbiamo compilare entrambi i file con il comando

```sh
$ cc -o random random_miomao.c random_main.c
```

#### Programmazione a oggetti

![oop 0](./oop_0.png)

![oop 1](./oop_1.png)

![l'utilizzo del paradigma *oop* da parte di `faust`](./faust_oop.png)
