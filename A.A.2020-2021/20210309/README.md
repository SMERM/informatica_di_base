# Bernardini - Fondamenti di Informatica - Seminario del 09/03/2021

[YouTube video](https://youtu.be/ksO0hhkIS-Q)

## Reti

* che cos'è una rete informatica? com'è fatta?
  ![configurazione di rete](./configurazione_di_rete.png)
* pacchettizzazione (TCP/UDP) e indirizzamento IP
  ![lavagna reti](./lavagna_reti.jpg)
* Modello client-server
  * esempio semplificativo e spiegato
  * architettura client-server
    ![client/server](./client_server.png)

## Domanda: che cos'è un VPN? (*Virtual Private Network*)

![vpn](./vpn.png)
