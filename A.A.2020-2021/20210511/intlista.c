#include <stdio.h>
#include <stdlib.h>
typedef struct int_lista
{
	struct int_lista *next;
	int value;
}Int_lista;


Int_lista* new_(int value)
{
	Int_lista*ret=(Int_lista *)malloc(sizeof(Int_lista));
	ret->value=value;
	return ret;
}

static Int_lista HEAD={(Int_lista*)NULL,0};

void append (Int_lista* obj)
{
	Int_lista*p=HEAD.next, *last=&HEAD;
	while(p!=(Int_lista*)NULL)
	{
		last=p;
		p=p->next;
	}
	obj->next=(Int_lista*)NULL;
	last->next=obj;
}

void insert(int index,Int_lista*obj)
{
	Int_lista*p=HEAD.next, *last=&HEAD;
	int i=0;
	while(i<index && p!=(Int_lista*)NULL)
	{
		++i;
		last=p;
		p=p->next;
	}
	obj->next=last->next;
	last->next=obj;
}

void print()
{
	Int_lista*p=HEAD.next;
	while(p!=(Int_lista*)NULL)
	{
		printf("%d\n",p->value);
		p=p->next;
	}
}

int main()
{
	append (new_(23));
	append (new_(24));
	append (new_(25));
	print();
	insert(1,new_(23));
	print();
}
