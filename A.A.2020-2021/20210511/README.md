# Bernardini - Fondamenti di Informatica - Seminario del 11/05/2021

## [YouTube video](https://youtu.be/DVyHKN2CCik)

## Programmazione

### Core memory: `text`, `memory`, `stack`

![Stack and allocation](./stack_e_alloc.png)

#### [esempio di Stack overflow](./stack.c)

```C
#include <stdio.h>


void do_the_stack_overflow()
{
	static int index=0;
	char buffer[0x1000];
	printf("%d: %p ",index, buffer);
	++index;
	do_the_stack_overflow();
}


int main()
{
	do_the_stack_overflow();

}
```

#### [esempio di memory overflow](./alloc.c)

```C
#include <malloc.h>
#include <stdio.h>


void *gonfia()
{
	int size=0x4000;
	return malloc(size);
}

int main()
{
	
	while(1)
		printf("%p", gonfia());

}
```

### Introduzione alle *linked list*

![*linked lists*](./linked_list.png)

Le *linked list* sono implementate automaticamente nei linguaggi di scripting
(p.es. [`python`](./lista.py) oppure `ruby` ecc.):

```python
a=[1,2,3,4,5]
print(a)
a.insert(3,23)
print(a)
```

Per capire come funzionano, implementiamo una lista di numeri interi in [`C`](./intlista.c)

```C
#include <stdio.h>
#include <stdlib.h>
typedef struct int_lista
{
	struct int_lista *next;
	int value;
}Int_lista;


Int_lista* new_(int value)
{
	Int_lista*ret=(Int_lista *)malloc(sizeof(Int_lista));
	ret->value=value;
	return ret;
}

static Int_lista HEAD={(Int_lista*)NULL,0};

void append (Int_lista* obj)
{
	Int_lista*p=HEAD.next, *last=&HEAD;
	while(p!=(Int_lista*)NULL)
	{
		last=p;
		p=p->next;
	}
	obj->next=(Int_lista*)NULL;
	last->next=obj;
}

void insert(int index,Int_lista*obj)
{
	Int_lista*p=HEAD.next, *last=&HEAD;
	int i=0;
	while(i<index && p!=(Int_lista*)NULL)
	{
		++i;
		last=p;
		p=p->next;
	}
	obj->next=last->next;
	last->next=obj;
}

void print()
{
	Int_lista*p=HEAD.next;
	while(p!=(Int_lista*)NULL)
	{
		printf("%d\n",p->value);
		p=p->next;
	}
}

int main()
{
	append (new_(23));
	append (new_(24));
	append (new_(25));
	print();
	insert(1,new_(23));
	print();
}
```
