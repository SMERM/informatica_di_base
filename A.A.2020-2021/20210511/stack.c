#include <stdio.h>


void do_the_stack_overflow()
{
	static int index=0;
	char buffer[0x1000];
	printf("%d: %p ",index, buffer);
	++index;
	do_the_stack_overflow();
}


int main()
{
	do_the_stack_overflow();

}
